#!/usr/bin/env/python3
from PIL import Image, ImageDraw
import random
import math
import sys


def main(x,y, name, dest, seed):
    # Constants
    SQUARE_SIZE = 200
    WIDTH, HEIGHT = (int(x))*SQUARE_SIZE + (int(x) + 1)*SQUARE_SIZE//4, (int(y))*SQUARE_SIZE + (int(y) + 1)*SQUARE_SIZE//4
    WALL_SIZE = SQUARE_SIZE // 4  # Make shapes smaller
    BACKGROUND_COLOR = (0,0,0)
    SHAPE_COLOR = (100, 100, 100)
    CHAIR_COLOR = (50, 50, 50)
    TABLE_COLOR = (75, 75, 75)
    WAll_CLEARING = 20
    random.seed(int(seed))
    # Create a new image with gray background
    image = Image.new("RGB", (WIDTH, HEIGHT), BACKGROUND_COLOR)
    draw = ImageDraw.Draw(image)
    #draw outer layer of walls
    draw.rectangle([0,0, WIDTH, SQUARE_SIZE//4],outline=SHAPE_COLOR, fill=SHAPE_COLOR)
    draw.rectangle([0,0, SQUARE_SIZE//4, HEIGHT],outline=SHAPE_COLOR, fill=SHAPE_COLOR)
    draw.rectangle([0,HEIGHT-SQUARE_SIZE//4, WIDTH, HEIGHT],outline=SHAPE_COLOR, fill=SHAPE_COLOR)
    draw.rectangle([WIDTH-SQUARE_SIZE//4,0, WIDTH, HEIGHT],outline=SHAPE_COLOR, fill=SHAPE_COLOR)

    room_num = random.randint(2, int(math.sqrt(int(x)*int(y))))

    rooms = []
    rooms.append([(0,0),(int(x),int(y))])
    rooms_area = []
    rooms_area.append((rooms[0][1][0]-rooms[0][0][0])*(rooms[0][1][1]-rooms[0][0][1]))
    max_room_ind = 0
    fail = 0
    i = 0
    while i < room_num:
        fail = 0
        dir = random.randint(0, 1)
        #vertical
        if dir == 1:
            w = rooms[max_room_ind][1][0]-rooms[max_room_ind][0][0]
            if w != 1:
                vert_slice = random.randint(1, w-1)
                rooms.append([rooms[max_room_ind][0],(rooms[max_room_ind][0][0]+vert_slice, rooms[max_room_ind][1][1])])
                rooms.append([(rooms[max_room_ind][0][0]+vert_slice, rooms[max_room_ind][0][1]),rooms[max_room_ind][1]])
                rooms.pop(max_room_ind)
            else:
                fail = 1
        #horizontal
        if dir == 0:
            h = rooms[max_room_ind][1][1]-rooms[max_room_ind][0][1]
            if h != 1:
                hor_slice = random.randint(1, h-1)
                rooms.append([rooms[max_room_ind][0],(rooms[max_room_ind][1][0], rooms[max_room_ind][0][1]+hor_slice)])
                rooms.append([(rooms[max_room_ind][0][0], rooms[max_room_ind][0][1]+hor_slice),rooms[max_room_ind][1]])
                rooms.pop(max_room_ind)
            else:
                fail = 1

        if fail == 0:
            #recalculate max_room_ind
            l = len(rooms)
            rooms_area.append((rooms[l-2][1][0]-rooms[l-2][0][0])*(rooms[l-2][1][1]-rooms[l-2][0][1]))
            rooms_area.append((rooms[l-1][1][0]-rooms[l-1][0][0])*(rooms[l-1][1][1]-rooms[l-1][0][1]))
            rooms_area.pop(max_room_ind)
            max_room_ind= max(range(len(rooms_area)), key=rooms_area.__getitem__)
            i +=1


    for room in rooms:
        x1=room[0][0]*(SQUARE_SIZE+SQUARE_SIZE//4)+SQUARE_SIZE//4
        y1=room[0][1]*(SQUARE_SIZE+SQUARE_SIZE//4)+SQUARE_SIZE//4
        x2=room[1][0]*(SQUARE_SIZE+SQUARE_SIZE//4)+SQUARE_SIZE//4
        y2=room[1][1]*(SQUARE_SIZE+SQUARE_SIZE//4)+SQUARE_SIZE//4
        draw.rectangle([x1-SQUARE_SIZE//4,y1-SQUARE_SIZE//4, x2, y1],fill=SHAPE_COLOR)
        draw.rectangle([x1-SQUARE_SIZE//4,y1-SQUARE_SIZE//4, x1, y2],fill=SHAPE_COLOR)
        draw.rectangle([x1-SQUARE_SIZE//4,y2-SQUARE_SIZE//4, x2, y2],fill=SHAPE_COLOR)
        draw.rectangle([x2-SQUARE_SIZE//4,y1-SQUARE_SIZE//4, x2, y2],fill=SHAPE_COLOR)

    #connect rooms by doors
        #vertical doors
        for i in range (int(x)-1):
            for j in range (int(y)):
                x1 = i*(SQUARE_SIZE+SQUARE_SIZE//4)+SQUARE_SIZE//4
                y1 = j*(SQUARE_SIZE+SQUARE_SIZE//4)+SQUARE_SIZE//4
                draw.rectangle([x1+SQUARE_SIZE,y1+3*SQUARE_SIZE//8, x1+SQUARE_SIZE+SQUARE_SIZE//4, y1+5*SQUARE_SIZE//8],fill=BACKGROUND_COLOR)
        for i in range (int(x)):
            for j in range (int(y)-1):
                x1 = i*(SQUARE_SIZE+SQUARE_SIZE//4)+SQUARE_SIZE//4
                y1 = j*(SQUARE_SIZE+SQUARE_SIZE//4)+SQUARE_SIZE//4
                draw.rectangle([x1+3*SQUARE_SIZE//8,y1+SQUARE_SIZE, x1+5*SQUARE_SIZE//8, y1+SQUARE_SIZE+SQUARE_SIZE//4],fill=BACKGROUND_COLOR)

    def draw_table_hor(x, y):
        draw.rectangle([x, y, x + 80, y + 40], fill=TABLE_COLOR)

    def draw_table_vert(x, y):
        draw.rectangle([x, y, x + 40, y + 80], fill=TABLE_COLOR)

    def draw_chair(x, y):
        draw.rectangle([x, y, x + 20, y + 20], fill=CHAIR_COLOR)

    def draw_shelf_hor(x, y):
        draw.rectangle([x, y, x + 100, y + 40], fill=SHAPE_COLOR)

    def draw_shelf_vert(x, y):
        draw.rectangle([x, y, x + 40, y + 100], fill=SHAPE_COLOR)

    #generate obstacles in rooms
    for room in rooms:
        x0=room[0][0]*(SQUARE_SIZE+SQUARE_SIZE//4)+SQUARE_SIZE//4
        y0=room[0][1]*(SQUARE_SIZE+SQUARE_SIZE//4)+SQUARE_SIZE//4
        w=room[1][0]-room[0][0]
        h=room[1][1]-room[0][1]

        for i in range(w):
            for j in range(h):
                x1 = x0 + i * (SQUARE_SIZE+SQUARE_SIZE//4)
                y1 = y0 + j * (SQUARE_SIZE+SQUARE_SIZE//4)
                room_decor = random.randint(0,10)
                #decor options
                if room_decor == 0:
                    draw_table_hor(x1+WAll_CLEARING+40, y1+WAll_CLEARING+50)
                    draw_chair(x1+WAll_CLEARING+60, y1+WAll_CLEARING+90)
                elif room_decor == 1:
                    draw_table_vert(x1+WAll_CLEARING+40, y1+WAll_CLEARING)
                    draw_chair(x1+WAll_CLEARING+80, y1+WAll_CLEARING+20)
                elif room_decor == 2:
                    draw_table_hor(x1+WAll_CLEARING+40, y1+WAll_CLEARING+90)
                    draw_chair(x1+WAll_CLEARING+60, y1+WAll_CLEARING+70)
                elif room_decor == 3:
                    draw_table_vert(x1+WAll_CLEARING+80, y1+WAll_CLEARING)
                    draw_chair(x1+WAll_CLEARING+60, y1+WAll_CLEARING+20)
                elif room_decor == 4:
                    draw_shelf_vert(x1+WAll_CLEARING+40, y1+WAll_CLEARING)
                elif room_decor == 5:
                    draw_shelf_hor(x1+WAll_CLEARING+40, y1+WAll_CLEARING)
                elif room_decor == 6:
                    draw_shelf_vert(x1+WAll_CLEARING+40, y1+WAll_CLEARING)
                    draw_shelf_vert(x1+WAll_CLEARING+100, y1+WAll_CLEARING)
                elif room_decor == 7:
                    draw_shelf_hor(x1+WAll_CLEARING+40, y1+WAll_CLEARING+40)
                    draw_shelf_hor(x1+WAll_CLEARING+40, y1+WAll_CLEARING+40+60)
                elif room_decor == 8:
                    draw_table_vert(x1+60, y1+50)
                    draw_chair(x1+40, y1+80)

                    draw_table_vert(x1+100, y1+50)
                    draw_chair(x1+140, y1+80)

                elif room_decor == 9:
                    draw_table_hor(x1+50, y1+60)
                    draw_chair(x1+80, y1+40)

                    draw_table_hor(x1+50, y1+100)
                    draw_chair(x1+80, y1+140)

    # Save the image
    if name == "preview":
        image.save(dest+"/"+name + ".jpg")
    else:
        image.save(dest+"/"+"["+str(seed)+"]"+name + ".jpg")

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
