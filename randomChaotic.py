#!/usr/bin/env/python3
from PIL import Image, ImageDraw
import random
import math
import sys
from polygenerator import (
    random_polygon,
    random_star_shaped_polygon,
    random_convex_polygon,
)

def polygonArea(pol, numPoints):
    X = []
    Y = []

    for p in pol:
        X.append(p[0])
        Y.append(p[1])

    area = 0
    j = numPoints-1

    for i in range(0, numPoints):
        area +=  (X[j]+X[i]) * (Y[j]-Y[i])
        j = i
    return abs(area/2);

def main(x,y, name, dest, seed, disp):
    # Constants
    SQUARE_SIZE = 200
    WIDTH, HEIGHT = (int(x)+1)*SQUARE_SIZE , (int(y)+1)*SQUARE_SIZE
    SHAPE_SIZE = SQUARE_SIZE // 4  # Make shapes smaller
    BACKGROUND_COLOR = (0,0,0)
    SHAPE_COLOR = (100, 100, 100)
    seed = int(seed)
    random.seed(seed)

    # Create a new image with gray background
    image = Image.new("RGB", (WIDTH+SQUARE_SIZE//2, HEIGHT+SQUARE_SIZE//2), BACKGROUND_COLOR)
    draw = ImageDraw.Draw(image)
    px = []
    py = []
    # Add random shapes
    points = []

    if disp =="Distant":
        MinDist = SQUARE_SIZE
        numObst = int(x)*int(y)//2

    if disp == "Medium":
        MinDist = SQUARE_SIZE//2
        numObst = int(x)*int(y)

    if disp == "Close":
        MinDist = SQUARE_SIZE//4
        numObst = int(x)*int(y)*2


    for i in range(numObst):

        flag = False
        x = random.randint(SQUARE_SIZE, WIDTH-SQUARE_SIZE)
        y = random.randint(SQUARE_SIZE, HEIGHT-SQUARE_SIZE)
        for p in points:
            if math.sqrt((x-p[0])**2 + (y - p[1])**2) < MinDist:
                flag = True

        while flag:
            flag = False
            x = random.randint(SQUARE_SIZE, WIDTH-SQUARE_SIZE)
            y = random.randint(SQUARE_SIZE, HEIGHT-SQUARE_SIZE)
            for p in points:
                if math.sqrt((x-p[0])**2 + (y - p[1])**2) < MinDist:
                    flag = True

        points.append([x,y])

        numPoints = random.randint(3,7)
        if numPoints !=7:
            polygon = random_star_shaped_polygon(num_points=numPoints)
            while(polygonArea(polygon, numPoints) < 0.3):
                polygon = random_star_shaped_polygon(num_points=numPoints)
            pol = []
            for p in polygon:
                pol.append((x+(SQUARE_SIZE // 2)*p[0], y + (SQUARE_SIZE // 2)*p[1]))
            draw.polygon(pol, fill = SHAPE_COLOR)

    draw.rectangle([0,0, WIDTH+SQUARE_SIZE//2, SQUARE_SIZE//2],outline=SHAPE_COLOR, fill=SHAPE_COLOR)
    draw.rectangle([0,0, SQUARE_SIZE//2, HEIGHT+SQUARE_SIZE//2],outline=SHAPE_COLOR, fill=SHAPE_COLOR)
    draw.rectangle([0,HEIGHT, WIDTH+SQUARE_SIZE//2, HEIGHT+SQUARE_SIZE//2],outline=SHAPE_COLOR, fill=SHAPE_COLOR)
    draw.rectangle([WIDTH,0, WIDTH+SQUARE_SIZE//2, HEIGHT+SQUARE_SIZE//2],outline=SHAPE_COLOR, fill=SHAPE_COLOR)

    # Save the image
    if name == "preview":
        image.save(dest+"/"+name + ".jpg")
    else:
        image.save(dest+"/"+"["+str(seed)+"]"+name + ".jpg")

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5],sys.argv[6])
