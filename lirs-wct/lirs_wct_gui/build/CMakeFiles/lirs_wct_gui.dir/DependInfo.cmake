# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/ilya/lirs-wct/lirs_wct_gui/build/include/moc_mainwindow.cpp" "/home/ilya/lirs-wct/lirs_wct_gui/build/CMakeFiles/lirs_wct_gui.dir/include/moc_mainwindow.cpp.o"
  "/home/ilya/lirs-wct/lirs_wct_gui/build/lirs_wct_gui_autogen/mocs_compilation.cpp" "/home/ilya/lirs-wct/lirs_wct_gui/build/CMakeFiles/lirs_wct_gui.dir/lirs_wct_gui_autogen/mocs_compilation.cpp.o"
  "/home/ilya/lirs-wct/lirs_wct_gui/src/main.cpp" "/home/ilya/lirs-wct/lirs_wct_gui/build/CMakeFiles/lirs_wct_gui.dir/src/main.cpp.o"
  "/home/ilya/lirs-wct/lirs_wct_gui/src/mainwindow.cpp" "/home/ilya/lirs-wct/lirs_wct_gui/build/CMakeFiles/lirs_wct_gui.dir/src/mainwindow.cpp.o"
  "/home/ilya/lirs-wct/lirs_wct_gui/src/utils.cpp" "/home/ilya/lirs-wct/lirs_wct_gui/build/CMakeFiles/lirs_wct_gui.dir/src/utils.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  "QT_WIDGETS_LIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "../"
  "lirs_wct_gui_autogen/include"
  "../include"
  "/usr/include/x86_64-linux-gnu/qt5"
  "/usr/include/x86_64-linux-gnu/qt5/QtCore"
  "/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++"
  "/usr/include/x86_64-linux-gnu/qt5/QtGui"
  "/usr/include/x86_64-linux-gnu/qt5/QtWidgets"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
