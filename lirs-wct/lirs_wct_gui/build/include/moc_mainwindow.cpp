/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.8)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../include/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.8. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[22];
    char stringdata0[595];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 29), // "on_convertStartButton_clicked"
QT_MOC_LITERAL(2, 41, 0), // ""
QT_MOC_LITERAL(3, 42, 28), // "on_selectImageButton_clicked"
QT_MOC_LITERAL(4, 71, 25), // "on_pngToDaeSelect_clicked"
QT_MOC_LITERAL(5, 97, 26), // "on_zAxisScaleLabel_clicked"
QT_MOC_LITERAL(6, 124, 30), // "on_dimensionScaleLabel_clicked"
QT_MOC_LITERAL(7, 155, 27), // "on_xAxisRotateLabel_clicked"
QT_MOC_LITERAL(8, 183, 29), // "on_outputFolderSelect_clicked"
QT_MOC_LITERAL(9, 213, 25), // "on_pngToStlSelect_clicked"
QT_MOC_LITERAL(10, 239, 25), // "on_stlToDaeSelect_clicked"
QT_MOC_LITERAL(11, 265, 29), // "on_textureImageSelect_clicked"
QT_MOC_LITERAL(12, 295, 34), // "on_smoothingOptionCheckbox_cl..."
QT_MOC_LITERAL(13, 330, 34), // "on_sharpnessOptionCheckbox_cl..."
QT_MOC_LITERAL(14, 365, 37), // "on_colorInverseOptionCheckbox..."
QT_MOC_LITERAL(15, 403, 34), // "on_grayscaleOptionCheckbox_cl..."
QT_MOC_LITERAL(16, 438, 17), // "onProcessFinished"
QT_MOC_LITERAL(17, 456, 16), // "onProcessStarted"
QT_MOC_LITERAL(18, 473, 29), // "onSmoothingSliderValueChanged"
QT_MOC_LITERAL(19, 503, 30), // "onSmoothingSpinBoxValueChanged"
QT_MOC_LITERAL(20, 534, 29), // "onSharpnessSliderValueChanged"
QT_MOC_LITERAL(21, 564, 30) // "onSharpnessSpinBoxValueChanged"

    },
    "MainWindow\0on_convertStartButton_clicked\0"
    "\0on_selectImageButton_clicked\0"
    "on_pngToDaeSelect_clicked\0"
    "on_zAxisScaleLabel_clicked\0"
    "on_dimensionScaleLabel_clicked\0"
    "on_xAxisRotateLabel_clicked\0"
    "on_outputFolderSelect_clicked\0"
    "on_pngToStlSelect_clicked\0"
    "on_stlToDaeSelect_clicked\0"
    "on_textureImageSelect_clicked\0"
    "on_smoothingOptionCheckbox_clicked\0"
    "on_sharpnessOptionCheckbox_clicked\0"
    "on_colorInverseOptionCheckbox_clicked\0"
    "on_grayscaleOptionCheckbox_clicked\0"
    "onProcessFinished\0onProcessStarted\0"
    "onSmoothingSliderValueChanged\0"
    "onSmoothingSpinBoxValueChanged\0"
    "onSharpnessSliderValueChanged\0"
    "onSharpnessSpinBoxValueChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  114,    2, 0x08 /* Private */,
       3,    0,  115,    2, 0x08 /* Private */,
       4,    0,  116,    2, 0x08 /* Private */,
       5,    0,  117,    2, 0x08 /* Private */,
       6,    0,  118,    2, 0x08 /* Private */,
       7,    0,  119,    2, 0x08 /* Private */,
       8,    0,  120,    2, 0x08 /* Private */,
       9,    0,  121,    2, 0x08 /* Private */,
      10,    0,  122,    2, 0x08 /* Private */,
      11,    0,  123,    2, 0x08 /* Private */,
      12,    0,  124,    2, 0x08 /* Private */,
      13,    0,  125,    2, 0x08 /* Private */,
      14,    0,  126,    2, 0x08 /* Private */,
      15,    0,  127,    2, 0x08 /* Private */,
      16,    1,  128,    2, 0x08 /* Private */,
      17,    0,  131,    2, 0x08 /* Private */,
      18,    1,  132,    2, 0x08 /* Private */,
      19,    1,  135,    2, 0x08 /* Private */,
      20,    1,  138,    2, 0x08 /* Private */,
      21,    1,  141,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Double,    2,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_convertStartButton_clicked(); break;
        case 1: _t->on_selectImageButton_clicked(); break;
        case 2: _t->on_pngToDaeSelect_clicked(); break;
        case 3: _t->on_zAxisScaleLabel_clicked(); break;
        case 4: _t->on_dimensionScaleLabel_clicked(); break;
        case 5: _t->on_xAxisRotateLabel_clicked(); break;
        case 6: _t->on_outputFolderSelect_clicked(); break;
        case 7: _t->on_pngToStlSelect_clicked(); break;
        case 8: _t->on_stlToDaeSelect_clicked(); break;
        case 9: _t->on_textureImageSelect_clicked(); break;
        case 10: _t->on_smoothingOptionCheckbox_clicked(); break;
        case 11: _t->on_sharpnessOptionCheckbox_clicked(); break;
        case 12: _t->on_colorInverseOptionCheckbox_clicked(); break;
        case 13: _t->on_grayscaleOptionCheckbox_clicked(); break;
        case 14: _t->onProcessFinished((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->onProcessStarted(); break;
        case 16: _t->onSmoothingSliderValueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->onSmoothingSpinBoxValueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 18: _t->onSharpnessSliderValueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->onSharpnessSpinBoxValueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 20)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 20;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
