# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/ilya/lirs-wct/lirs_wct_console/src/Mesh.cpp" "/home/ilya/lirs-wct/lirs_wct_console/build/CMakeFiles/lirs_wct_console.dir/src/Mesh.cpp.o"
  "/home/ilya/lirs-wct/lirs_wct_console/src/ModelLoader.cpp" "/home/ilya/lirs-wct/lirs_wct_console/build/CMakeFiles/lirs_wct_console.dir/src/ModelLoader.cpp.o"
  "/home/ilya/lirs-wct/lirs_wct_console/src/heightmap.cpp" "/home/ilya/lirs-wct/lirs_wct_console/build/CMakeFiles/lirs_wct_console.dir/src/heightmap.cpp.o"
  "/home/ilya/lirs-wct/lirs_wct_console/src/libtrix.cpp" "/home/ilya/lirs-wct/lirs_wct_console/build/CMakeFiles/lirs_wct_console.dir/src/libtrix.cpp.o"
  "/home/ilya/lirs-wct/lirs_wct_console/src/main.cpp" "/home/ilya/lirs-wct/lirs_wct_console/build/CMakeFiles/lirs_wct_console.dir/src/main.cpp.o"
  "/home/ilya/lirs-wct/lirs_wct_console/src/png2dae.cpp" "/home/ilya/lirs-wct/lirs_wct_console/build/CMakeFiles/lirs_wct_console.dir/src/png2dae.cpp.o"
  "/home/ilya/lirs-wct/lirs_wct_console/src/png2stl.cpp" "/home/ilya/lirs-wct/lirs_wct_console/build/CMakeFiles/lirs_wct_console.dir/src/png2stl.cpp.o"
  "/home/ilya/lirs-wct/lirs_wct_console/src/script_generator.cpp" "/home/ilya/lirs-wct/lirs_wct_console/build/CMakeFiles/lirs_wct_console.dir/src/script_generator.cpp.o"
  "/home/ilya/lirs-wct/lirs_wct_console/src/utils.cpp" "/home/ilya/lirs-wct/lirs_wct_console/build/CMakeFiles/lirs_wct_console.dir/src/utils.cpp.o"
  "/home/ilya/lirs-wct/lirs_wct_console/src/world_creator.cpp" "/home/ilya/lirs-wct/lirs_wct_console/build/CMakeFiles/lirs_wct_console.dir/src/world_creator.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BOOST_ALL_NO_LIB"
  "BOOST_FILESYSTEM_DYN_LINK"
  "BOOST_PROGRAM_OPTIONS_DYN_LINK"
  "BOOST_SYSTEM_DYN_LINK"
  "MAGICKCORE_HDRI_ENABLE=0"
  "MAGICKCORE_QUANTUM_DEPTH=16"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "/usr/include/eigen3"
  "/usr/include/ImageMagick-6"
  "/usr/include/x86_64-linux-gnu/ImageMagick-6"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
