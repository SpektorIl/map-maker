# This Python file uses the following encoding: utf-8
#!/usr/bin/env/python3
import sys
import os
import subprocess
import csv
import PyQt5
import roslaunch
from PySide2.QtWidgets import QApplication
from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtWidgets import *
import time
import random

#class of signals to emit when the progress has completed or map made
class Signals(QtCore.QObject):
    completed = QtCore.pyqtSignal()
    mapmade = QtCore.pyqtSignal(int, int)
    preview = QtCore.pyqtSignal(str)


#worker to generate maps
class WorkerGenerator(QtCore.QRunnable):
    #initialize variables
    def __init__(self, flag, map_type, amount,x,y, mname, fname, desttext, seed, wall, disp):
        super(WorkerGenerator,self).__init__()
        self.mname = mname
        self.fname = fname
        self.desttext = desttext
        self.x = x
        self.y = y
        self.amount = amount
        self.m_type = map_type
        self.l_flag = flag
        self.signals = Signals()
        self.seed = seed
        self.wall = wall
        self.disp = disp

        if self.seed == "":
            self.seed = random.randint(10000,99999)
        self.seed = str(self.seed)
    #run worker to generate maps
    @QtCore.pyqtSlot()
    def run(self):
        #generate amount of maps
        for i in range(self.amount):

           #generate mazes
           if self.m_type == 'Maze':
               #create 2Dmap
               os.system("python maze.py "+ str(self.x) + " " + str(self.y) + " " + self.mname + str(i+1)+" "+self.fname+" "+self.seed+" "+self.wall)
               #create 3D worlds
               if self.desttext =='':
                   os.system("cd lirs-wct/release && ./lirs_wct_console -i"+ self.fname+ "/" +"["+str(self.seed)+"]"+ self.mname + str(i+1)+".jpg" +" -o "+self.mname + str(i+1)+" -f "+self.fname+" -k")
               else:
                   os.system("cd lirs-wct/release && ./lirs_wct_console -i"+ self.fname+ "/" +"["+str(self.seed)+"]"+ self.mname + str(i+1)+".jpg" +" -o "+self.mname + str(i+1)+" -f "+self.fname+" -t "+self.desttext+" -k")
               os.system("cd "+self.fname+" && mv generated_world.world "+ self.mname + str(i+1)+"_world.world")

                #create launch file if flag true
               if self.l_flag == True:
                   os.system("python launch_writer.py 0 "+self.mname + str(i+1)+' '+self.fname)


            #generate individual obstacles
           if self.m_type == 'Convex Obstacles Grid':
               #create 2Dmap
               os.system("python convexGrid.py "+ str(self.x) + " " + str(self.y) + " " + self.mname + str(i+1)+" "+self.fname+" "+self.seed)
               #create 3D worlds
               if self.desttext =='':
                   os.system("cd lirs-wct/release && ./lirs_wct_console -i"+ self.fname+ "/" +"["+str(self.seed)+"]"+ self.mname + str(i+1)+".jpg" +" -o "+self.mname + str(i+1)+" -f "+self.fname+" -k")
               else:
                   os.system("cd lirs-wct/release && ./lirs_wct_console -i"+ self.fname+ "/" +"["+str(self.seed)+"]"+ self.mname + str(i+1)+".jpg" +" -o "+self.mname + str(i+1)+" -f "+self.fname+" -t "+self.desttext+" -k")
               os.system("cd "+self.fname+" && mv generated_world.world "+ self.mname + str(i+1)+"_world.world")

                #create launch file if flag true
               if self.l_flag == True:
                   os.system("python launch_writer.py 0 "+self.mname + str(i+1)+' '+self.fname)

           if self.m_type == 'Convex Obstacles Chaotic':
               #create 2Dmap
               os.system("python convexChaotic.py "+ str(self.x) + " " + str(self.y) + " " + self.mname + str(i+1)+" "+self.fname+" "+self.seed+" "+self.disp)
               #create 3D worlds
               if self.desttext =='':
                   os.system("cd lirs-wct/release && ./lirs_wct_console -i"+ self.fname+ "/" +"["+str(self.seed)+"]"+ self.mname + str(i+1)+".jpg" +" -o "+self.mname + str(i+1)+" -f "+self.fname+" -k")
               else:
                   os.system("cd lirs-wct/release && ./lirs_wct_console -i"+ self.fname+ "/" +"["+str(self.seed)+"]"+ self.mname + str(i+1)+".jpg" +" -o "+self.mname + str(i+1)+" -f "+self.fname+" -t "+self.desttext+" -k")
               os.system("cd "+self.fname+" && mv generated_world.world "+ self.mname + str(i+1)+"_world.world")

                #create launch file if flag true
               if self.l_flag == True:
                   os.system("python launch_writer.py 0 "+self.mname + str(i+1)+' '+self.fname)

           if self.m_type == 'Convex and Concave Obstacles Grid':
               #create 2Dmap
               os.system("python randomGrid.py "+ str(self.x) + " " + str(self.y) + " " + self.mname + str(i+1)+" "+self.fname+" "+self.seed)
               #create 3D worlds
               if self.desttext =='':
                   os.system("cd lirs-wct/release && ./lirs_wct_console -i"+ self.fname+ "/" +"["+str(self.seed)+"]"+ self.mname + str(i+1)+".jpg" +" -o "+self.mname + str(i+1)+" -f "+self.fname+" -k")
               else:
                   os.system("cd lirs-wct/release && ./lirs_wct_console -i"+ self.fname+ "/" +"["+str(self.seed)+"]"+ self.mname + str(i+1)+".jpg" +" -o "+self.mname + str(i+1)+" -f "+self.fname+" -t "+self.desttext+" -k")
               os.system("cd "+self.fname+" && mv generated_world.world "+ self.mname + str(i+1)+"_world.world")

                #create launch file if flag true
               if self.l_flag == True:
                   os.system("python launch_writer.py 0 "+self.mname + str(i+1)+' '+self.fname)

           if self.m_type == 'Convex and Concave Obstacles Chaotic':
               #create 2Dmap
               os.system("python randomChaotic.py "+ str(self.x) + " " + str(self.y) + " " + self.mname + str(i+1)+" "+self.fname+" "+self.seed+" "+self.disp)
               if self.desttext =='':
                   os.system("cd lirs-wct/release && ./lirs_wct_console -i"+ self.fname+ "/" +"["+str(self.seed)+"]"+ self.mname + str(i+1)+".jpg" +" -o "+self.mname + str(i+1)+" -f "+self.fname+" -k")
               else:
                   os.system("cd lirs-wct/release && ./lirs_wct_console -i"+ self.fname+ "/" +"["+str(self.seed)+"]"+ self.mname + str(i+1)+".jpg" +" -o "+self.mname + str(i+1)+" -f "+self.fname+" -t "+self.desttext+" -k")
               os.system("cd "+self.fname+" && mv generated_world.world "+ self.mname + str(i+1)+"_world.world")

                #create launch file if flag true
               if self.l_flag == True:
                   os.system("python launch_writer.py 0 "+self.mname + str(i+1)+' '+self.fname)

           if self.m_type == 'Large Individual Obstacles':
               #create 2Dmap
               os.system("python largeObst.py "+ str(self.x) + " " + str(self.y) + " " + self.mname + str(i+1)+" "+self.fname+" "+self.seed+" "+self.disp)
               if self.desttext =='':
                   os.system("cd lirs-wct/release && ./lirs_wct_console -i"+ self.fname+ "/" +"["+str(self.seed)+"]"+ self.mname + str(i+1)+".jpg" +" -o "+self.mname + str(i+1)+" -f "+self.fname+" -k")
               else:
                   os.system("cd lirs-wct/release && ./lirs_wct_console -i"+ self.fname+ "/" +"["+str(self.seed)+"]"+ self.mname + str(i+1)+".jpg" +" -o "+self.mname + str(i+1)+" -f "+self.fname+" -t "+self.desttext+" -k")
               os.system("cd "+self.fname+" && mv generated_world.world "+ self.mname + str(i+1)+"_world.world")

                #create launch file if flag true
               if self.l_flag == True:
                   os.system("python launch_writer.py 0 "+self.mname + str(i+1)+' '+self.fname)

           if self.m_type == 'Office Environments':
               #create 2Dmap
               os.system("python officeEnv.py "+ str(self.x) + " " + str(self.y) + " " + self.mname + str(i+1)+" "+self.fname+" "+self.seed)
               if self.desttext =='':
                   os.system("cd lirs-wct/release && ./lirs_wct_console -i"+ self.fname+ "/" +"["+str(self.seed)+"]"+ self.mname + str(i+1)+".jpg" +" -o "+self.mname + str(i+1)+" -f "+self.fname+" -k")
               else:
                   os.system("cd lirs-wct/release && ./lirs_wct_console -i"+ self.fname+ "/" +"["+str(self.seed)+"]"+ self.mname + str(i+1)+".jpg" +" -o "+self.mname + str(i+1)+" -f "+self.fname+" -t "+self.desttext+" -k")
               os.system("cd "+self.fname+" && mv generated_world.world "+ self.mname + str(i+1)+"_world.world")

                #create launch file if flag true
               if self.l_flag == True:
                   os.system("python launch_writer.py 0 "+self.mname + str(i+1)+' '+self.fname)
           self.seed = int(self.seed) + 1
           self.seed = str(self.seed)
            #emit signal when a map has been created to move progress bar
           self.signals.mapmade.emit(i, self.amount)
        #emit signal when all the maps were created
        self.signals.completed.emit()
    #close worker
    def close(self):
        return 1
#worker to generate maps
class WorkerPreview(QtCore.QRunnable):
    #initialize variables
    def __init__(self, map_type, x,y, seed, wall, disp):
        super(WorkerPreview,self).__init__()
        self.x = x
        self.y = y
        self.m_type = map_type
        self.signals = Signals()
        self.seed = seed
        self.wall = wall
        self.disp = disp
        self.mname = "preview"
        self.fname = os.getcwd()
        self.signals = Signals()
        if self.seed == "":
            self.seed = random.randint(10000,99999)
        self.seed = str(self.seed)
    #run worker to generate maps
    @QtCore.pyqtSlot()
    def run(self):
       #generate mazes
       if self.m_type == 'Maze':
           #create 2Dmap
           os.system("python maze.py "+ str(self.x) + " " + str(self.y) + " " + self.mname+" "+self.fname+" "+self.seed+" "+self.wall)

        #generate individual obstacles
       if self.m_type == 'Convex Obstacles Grid':
           #create 2Dmap
           os.system("python convexGrid.py "+ str(self.x) + " " + str(self.y) + " " + self.mname+" "+self.fname+" "+self.seed)

       if self.m_type == 'Convex Obstacles Chaotic':
           #create 2Dmap
           os.system("python convexChaotic.py "+ str(self.x) + " " + str(self.y) + " " + self.mname+" "+self.fname+" "+self.seed+" "+self.disp)

       if self.m_type == 'Convex and Concave Obstacles Grid':
           #create 2Dmap
           os.system("python randomGrid.py "+ str(self.x) + " " + str(self.y) + " " + self.mname+" "+self.fname+" "+self.seed)

       if self.m_type == 'Convex and Concave Obstacles Chaotic':
           #create 2Dmap
           os.system("python randomChaotic.py "+ str(self.x) + " " + str(self.y) + " " + self.mname+" "+self.fname+" "+self.seed+" "+self.disp)

       if self.m_type == 'Large Individual Obstacles':
           #create 2Dmap
           os.system("python largeObst.py "+ str(self.x) + " " + str(self.y) + " " + self.mname+" "+self.fname+" "+self.seed+" "+self.disp)

       if self.m_type == 'Office Environments':
           #create 2Dmap
           os.system("python officeEnv.py "+ str(self.x) + " " + str(self.y) + " " + self.mname +" "+self.fname+" "+self.seed)

       self.signals.preview.emit(self.fname+"/"+"["+str(self.seed)+"]"+self.mname + ".jpg")
    #close worker
    def close(self):
        return 1

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        #function to launch world generation
        def generate():
            #get destination ditectory name
            fname = self.dest_file.text()
            #get texture file name
            desttext = self.text_line.text()
            #get map name
            mname = self.map_name.text()
            #check if directory was written into line
            seed = self.seed_line.text()
            wall = self.wall_cb.currentText()
            disp = self.disp_cb.currentText()
            if fname == '':
                msg = QMessageBox(self)
                msg.setWindowTitle("Error!")
                msg.setText("Choose a final directory file, please!")
                msg.setIcon(QMessageBox.Warning)
                msg.exec_()
            else:
                #check if map name was written into a line
                if mname == '':
                    msg2 = QMessageBox(self)
                    msg2.setWindowTitle("Error!")
                    msg2.setText("Name your maps, please!")
                    msg2.setIcon(QMessageBox.Warning)
                    msg2.exec_()
                else:
                    if (seed.isdigit() and len(seed) == 5) or seed == '':
                        #proceed to generate worlds
                        #get x parameter
                        x = self.x_spinBox.value()
                        #get y parameter
                        y = self.y_spinBox.value()
                        #check if it is required to create launch file
                        flag = self.checkBox.isChecked()
                        #disable generate button
                        self.generate.setEnabled(False)
                        #activate threadpool
                        threadpool = QtCore.QThreadPool.globalInstance()
                        #get amount of maps to create
                        amount = self.amount_map_cb.value()
                        #get map type
                        map_type = self.type_comboBox.currentText()
                        #activate worker thread
                        self.workergen = WorkerGenerator(flag, map_type, amount,x,y, mname, fname, desttext, seed, wall, disp)
                        #connect signals
                        self.workergen.signals.completed.connect(self.release)
                        self.workergen.signals.mapmade.connect(self.update)
                        #connect worker to threadpool
                        threadpool.start(self.workergen)
                    else:
                        msg3 = QMessageBox(self)
                        msg3.setWindowTitle("Error!")
                        msg3.setText("Seeds must be five digits long!")
                        msg3.setIcon(QMessageBox.Warning)
                        msg3.exec_()
                        #function to launch world generation
        def preview():
            #check if directory was written into line
            seed = self.seed_line.text()
            wall = self.wall_cb.currentText()
            disp = self.disp_cb.currentText()
            if (seed.isdigit() and len(seed) == 5) or seed == '':
                #get x parameter
                x = self.x_spinBox.value()
                #get y parameter
                y = self.y_spinBox.value()
                #disable generate button
                self.pushButton.setEnabled(False)
                #activate threadpool
                threadpool = QtCore.QThreadPool.globalInstance()
                #get map type
                map_type = self.type_comboBox.currentText()
                #activate worker thread
                self.workerpre = WorkerPreview(map_type, x,y,seed, wall, disp)
                self.workerpre.signals.preview.connect(self.showpreview)
                threadpool.start(self.workerpre)
            else:
                msg3 = QMessageBox(self)
                msg3.setWindowTitle("Error!")
                msg3.setText("Seeds must be five digits long!")
                msg3.setIcon(QMessageBox.Warning)
                msg3.exec_()


        #Get texture and show it in a small window
        def browsetexture():
            fname = QFileDialog.getOpenFileName(self,'Open texture image',"../",'Images (*.jpg *.png)')
            self.text_line.setText(fname[0])
            self.dialog = QtWidgets.QDialog(self)
            self.dialog.setLayout(QtWidgets.QVBoxLayout())
            label = QtWidgets.QLabel(self.dialog)
            pixmap = QtGui.QPixmap(fname[0])
            label.setPixmap(pixmap)
            label.resize(pixmap.width()//4,pixmap.height()//4)
            self.dialog.resize(pixmap.width()//4,pixmap.height()//4)
            self.dialog.show()
            label.show()
        # get final directory name and write it into dest_file line edit
        def browsefiles():
            dname = QFileDialog.getExistingDirectory(self,'Open launch file',"../", QFileDialog.ShowDirsOnly)
            self.dest_file.setText(dname)

        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(552, 678)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 0, 271, 131))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_9 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_9.setObjectName("label_9")

        self.verticalLayout.addWidget(self.label_9)

        self.type_comboBox = QtWidgets.QComboBox(self.verticalLayoutWidget)
        self.type_comboBox.setObjectName("type_comboBox")
        self.type_comboBox.addItem("")
        self.type_comboBox.addItem("")
        self.type_comboBox.addItem("")
        self.type_comboBox.addItem("")
        self.type_comboBox.addItem("")
        self.type_comboBox.addItem("")
        self.type_comboBox.addItem("")
        self.type_comboBox.currentTextChanged.connect(self.update_param)

        self.verticalLayout.addWidget(self.type_comboBox)
        self.label_14 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_14.setObjectName("label_14")

        self.verticalLayout.addWidget(self.label_14)

        self.formLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.formLayoutWidget.setGeometry(QtCore.QRect(10, 130, 271, 91))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtWidgets.QFormLayout(self.formLayoutWidget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")

        self.label_3 = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_3.setObjectName("label_3")

        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_3)

        self.y_spinBox = QtWidgets.QSpinBox(self.formLayoutWidget)
        self.y_spinBox.setObjectName("y_spinBox")
        self.y_spinBox.setValue(10)
        self.y_spinBox.setMinimum(3)
        self.y_spinBox.setMaximum(30)

        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.y_spinBox)

        self.x_spinBox = QtWidgets.QSpinBox(self.formLayoutWidget)
        self.x_spinBox.setObjectName("x_spinBox")
        self.x_spinBox.setValue(10)
        self.x_spinBox.setMinimum(3)
        self.x_spinBox.setMaximum(30)

        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.x_spinBox)

        self.label_4 = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_4.setObjectName("label_4")

        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_4)

        self.label_5 = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_5.setObjectName("label_5")

        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_5)

        self.seed_line = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.seed_line.setObjectName("seed_line")

        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.seed_line)

        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(330, 10, 270, 190))
        filename = "preview0.png"
        image = QtGui.QImage(filename)
        pp = QtGui.QPixmap.fromImage(image)
        pp = pp.scaled(200, 200)
        self.label_6.setPixmap(pp)
        self.label_6.setObjectName("label_6")

        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(365, 200, 131, 25))
        self.pushButton.clicked.connect(preview)
        self.pushButton.setObjectName("pushButton")

        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setGeometry(QtCore.QRect(10, 280, 231, 21))
        self.label_7.setObjectName("label_7")

        self.dest_file = QtWidgets.QLineEdit(self.centralwidget)
        self.dest_file.setGeometry(QtCore.QRect(10, 320, 431, 25))
        self.dest_file.setObjectName("dest_line")

        self.browses_dest = QtWidgets.QPushButton(self.centralwidget)
        self.browses_dest.setGeometry(QtCore.QRect(450, 320, 89, 25))
        self.browses_dest.clicked.connect(browsefiles)
        self.browses_dest.setObjectName("browses_dest")

        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setGeometry(QtCore.QRect(10, 360, 211, 31))

        self.label_8.setObjectName("label_8")

        self.stackedWidget = QtWidgets.QStackedWidget(self.centralwidget)
        self.stackedWidget.setGeometry(QtCore.QRect(10, 230, 591, 31))
        self.stackedWidget.setObjectName("stackedWidget")
        self.page = QtWidgets.QWidget()
        self.page.setObjectName("page")

        self.stackedWidget.addWidget(self.page)
        self.page_2 = QtWidgets.QWidget()
        self.page_2.setObjectName("page_2")

        self.label_12 = QtWidgets.QLabel(self.page_2)
        self.label_12.setGeometry(QtCore.QRect(0, 0, 121, 31))
        self.label_12.setObjectName("label_12")

        self.wall_cb = QtWidgets.QComboBox(self.page_2)
        self.wall_cb.addItem("")
        self.wall_cb.addItem("")
        self.wall_cb.addItem("")
        self.wall_cb.setCurrentIndex(1)
        self.wall_cb.setGeometry(QtCore.QRect(110, 0, 161, 31))
        self.wall_cb.setObjectName("wall_cb")

        self.stackedWidget.addWidget(self.page_2)
        self.page_3 = QtWidgets.QWidget()
        self.page_3.setObjectName("page_3")

        self.label_13 = QtWidgets.QLabel(self.page_3)
        self.label_13.setGeometry(QtCore.QRect(0, 10, 141, 17))
        self.label_13.setObjectName("label_13")

        self.disp_cb = QtWidgets.QComboBox(self.page_3)
        self.disp_cb.setGeometry(QtCore.QRect(150, 0, 121, 31))
        self.disp_cb.addItem("")
        self.disp_cb.addItem("")
        self.disp_cb.addItem("")
        self.disp_cb.setCurrentIndex(1)
        self.disp_cb.setObjectName("disp_cb")

        self.stackedWidget.addWidget(self.page_3)

        self.amount_map_cb = QtWidgets.QSpinBox(self.centralwidget)
        self.amount_map_cb.setGeometry(QtCore.QRect(220, 360, 61, 26))
        self.amount_map_cb.setValue(3)
        self.amount_map_cb.setMinimum(1)
        self.amount_map_cb.setMaximum(30)
        self.amount_map_cb.setObjectName("amount_map_cb")

        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(10, 400, 121, 17))
        self.label.setObjectName("label")

        self.map_name = QtWidgets.QLineEdit(self.centralwidget)
        self.map_name.setGeometry(QtCore.QRect(10, 430, 531, 25))
        self.map_name.setObjectName("map_name")

        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setGeometry(QtCore.QRect(10, 470, 241, 17))
        self.label_10.setObjectName("label_10")

        self.checkBox = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBox.setGeometry(QtCore.QRect(260, 470, 92, 16))
        self.checkBox.setText("")
        self.checkBox.setObjectName("checkBox")

        self.label_11 = QtWidgets.QLabel(self.centralwidget)
        self.label_11.setGeometry(QtCore.QRect(10, 500, 211, 17))
        self.label_11.setObjectName("label_11")

        self.text_line = QtWidgets.QLineEdit(self.centralwidget)
        self.text_line.setGeometry(QtCore.QRect(10, 530, 431, 25))
        self.text_line.setObjectName("text_line")

        self.browse_texture = QtWidgets.QPushButton(self.centralwidget)
        self.browse_texture.setGeometry(QtCore.QRect(450, 530, 89, 25))
        self.browse_texture.clicked.connect(browsetexture)
        self.browse_texture.setObjectName("browse_texture")

        self.generate = QtWidgets.QPushButton(self.centralwidget)
        self.generate.setGeometry(QtCore.QRect(380, 570, 161, 25))
        self.generate.clicked.connect(generate)
        self.generate.setObjectName("generate")

        self.pb_gen = QtWidgets.QProgressBar(self.centralwidget)
        self.pb_gen.setGeometry(QtCore.QRect(380, 600, 161, 23))
        self.pb_gen.setProperty("value", 0)
        self.pb_gen.setObjectName("pb_gen")

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 552, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)

        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.stackedWidget.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Map Maker"))
        self.label_9.setText(_translate("MainWindow", "Choose World Type"))
        self.label_14.setText(_translate("MainWindow", "Input World Size"))
        self.type_comboBox.setItemText(0, _translate("MainWindow", "Maze"))
        self.type_comboBox.setItemText(1, _translate("MainWindow", "Convex Obstacles Grid"))
        self.type_comboBox.setItemText(2, _translate("MainWindow", "Convex Obstacles Chaotic"))
        self.type_comboBox.setItemText(3, _translate("MainWindow", "Convex and Concave Obstacles Grid"))
        self.type_comboBox.setItemText(4, _translate("MainWindow", "Convex and Concave Obstacles Chaotic"))
        self.type_comboBox.setItemText(5, _translate("MainWindow", "Large Individual Obstacles"))
        self.type_comboBox.setItemText(6, _translate("MainWindow", "Office Environments"))
        self.wall_cb.setItemText(0, _translate("MainWindow", "Thick"))
        self.wall_cb.setItemText(1, _translate("MainWindow", "Medium"))
        self.wall_cb.setItemText(2, _translate("MainWindow", "Thin"))
        self.disp_cb.setItemText(0, _translate("MainWindow", "Distant"))
        self.disp_cb.setItemText(1, _translate("MainWindow", "Medium"))
        self.disp_cb.setItemText(2, _translate("MainWindow", "Close"))
        self.label_3.setText(_translate("MainWindow", "X"))
        self.label_4.setText(_translate("MainWindow", "Y"))
        self.label_5.setText(_translate("MainWindow", "Seed"))
        self.pushButton.setText(_translate("MainWindow", "Preview Map"))
        self.label_7.setText(_translate("MainWindow", "Choose Launch File Destination"))
        self.browses_dest.setText(_translate("MainWindow", "Browse"))
        self.label_8.setText(_translate("MainWindow", "Amount of Maps to Generate"))
        self.label_12.setText(_translate("MainWindow", "Wall Thickness"))
        self.label_13.setText(_translate("MainWindow", "Obstacle Configuration"))
        self.label.setText(_translate("MainWindow", "Input Map Name"))
        self.label_10.setText(_translate("MainWindow", "Create *.launch File for Turtlebot3"))
        self.label_11.setText(_translate("MainWindow", "Choose Texture File(Optional)"))
        self.browse_texture.setText(_translate("MainWindow", "Browse"))
        self.generate.setText(_translate("MainWindow", "Generate"))

#main window class
class Window(QMainWindow, Ui_MainWindow):
    #initialize main window and setuo its UI
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
    #function to show message when all maps were created
    def release(self):
        self.generate.setEnabled(True)
        self.pb_gen.setProperty("value", 0)
        msg3 = QMessageBox(self)
        msg3.setWindowTitle("Success!")
        msg3.setText("Programm Finished!")
        msg3.setIcon(QMessageBox.Information)
        msg3.exec_()
    def update_param(self):
        if self.type_comboBox.currentIndex() == 1 or self.type_comboBox.currentIndex() == 3 or self.type_comboBox.currentIndex() == 6:
            self.stackedWidget.setCurrentIndex(0)
        elif self.type_comboBox.currentIndex() == 0:
            self.stackedWidget.setCurrentIndex(1)
        else:
            self.stackedWidget.setCurrentIndex(2)


    #update progress bar
    def update(self,i, n):
        window.pb_gen.setProperty("value", 100*(i+1)//n)

    #update preview
    def showpreview(self, filename):
        image = QtGui.QImage("preview.jpg")
        pp = QtGui.QPixmap.fromImage(image)
        pp = pp.scaled(200, 200)
        window.label_6.setPixmap(pp)
        self.pushButton.setEnabled(True)


if __name__ == "__main__":
    #launch application
    app = QApplication([])
    #create main
    window = Window()
    #show window
    window.show()
    #execute app and exit when app is closed
    sys.exit(app.exec_())
